@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Materias por Alumno <span class="float-right"> <a href="/materias/create" class="btn btn-secondary">Registrar materia</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Alumno: {{ Auth::user()->name }}</h3>
                        @if (count($materias))
                            <table class="table table-striped">
                            <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Créditos</th>                                    
                                    </tr>
                                    </thead>
                                @foreach($materias as $materia)
                                    <tr>
                                        <td>{{$materia->nombre}}</td>
                                        <td>{{$materia->creditos}}</td>
                                        <td>
                                            <form class="float-right ml-2" action="/materias/{{$materia->id}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" name="button" class="btn btn-danger">Eliminar</button>
                                            </form>
                                        <a href="/materias/{{$materia->id}}/edit" class="btn btn-info float-right">Editar</a>
                                        </td>
                                    </tr>
                                    @endforeach
                            </table>
                        @else
                            <p>Aun no tienes materias asignadas</p>
                        @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
