@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar materia<span class="float-right"><a href="/home" class="btn btn-secondary">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/materias">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre de la materia</label>
                            <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                        </div>

                        <div class="form-group">
                            <label for="name">Créditos</label>
                            <input type="text" name="creditos" class="form-control" placeholder="Créditos">
                        </div>    

                        <button type="submit"  name="submit" class="btn btn-info float-left">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection



