@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modificar Materia<span class="float-right"><a href="/home" class="btn btn-secondary">Cancelar</a></span></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/materias/{{$materia->id }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{ $materia->nombre }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Créditos</label>
                            <input type="text" name="creditos" class="form-control" placeholder="Créditos" value="{{ $materia->creditos }}">
                        </div>                    
                        <button type="submit"  name="submit" class="btn btn-info float-left">Modificar</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
