<?php

namespace App\Http\Controllers;

use App\Http\Requests\MateriaRequest;
use Illuminate\Http\Request;
use App\Models\Materia;
use Illuminate\Support\Facades\Auth;

class MateriasController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MateriaRequest $request)
    {
        $this->validate($request, [
            'nombre' => 'Required',
            'creditos' => 'Required',
            
            
        ]);

        $materia = new Materia();
        $materia->user_id = Auth::id();
        $materia->nombre = $request->input('nombre');
        $materia->creditos = $request->input('creditos');   
        $materia->save();
        
        
        return redirect()->to('/home')->with('Bien', 'Materia registrada correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materia = Materia::find($id);
        return view('edit')->with('materia', $materia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'Required',
            'creditos' => 'Required'           
        ]);

        $materia = Materia::find($id);
        $materia->nombre = $request->input('nombre');
        $materia->creditos = $request->input('creditos');        
        $materia->save();

        return redirect()->to('/home')->with('Bien', 'Materia editada correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materia = Materia::find($id);
        $materia->delete();
        return redirect()->to('/home')->with('Bien', 'Materia eliminada correctamente!');
    }
}
